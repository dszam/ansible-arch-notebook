#!/bin/sh

(echo -e "---\npacman_packages:" ;pacman -Qqet | grep -v "$(pacman -Qqm)" | grep -v -e "manjaro\|linux" | awk '{print "  - " $1}') > ./vars/pacman.yml
(echo -e "---\naur_packages:";pacman -Qqm | awk '{print "  - " $1}') > ./vars/aur.yml
(echo -e "---\nvscode_plugins:";code --list-extensions | awk '{print "  - " $1}') > ./vars/vscode.yml