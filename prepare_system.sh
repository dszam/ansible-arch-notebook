#!/bin/sh

echo "installing ansible"
sudo pacman -S ansible

echo "installing ansible-aur"
yay -S --useask --answerclean All --answerdiff None ansible-aur-git

echo "updating system"
sudo pacman -Syu

echo "now run ansible-playbook -K install_packages.yml to install the packages in need automatically"
echo "after that, install configs by running ansible-playbook -K install_configs.yml"